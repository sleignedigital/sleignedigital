$(document).ready(function(){

  /*-------------------------------------------------------------------------------
    PRE LOADER
  -------------------------------------------------------------------------------*/

  $(window).load(function(){
    $('.preloader').fadeOut(1000); // set duration in brackets    
  });


  /*-------------------------------------------------------------------------------
    jQuery Parallax
  -------------------------------------------------------------------------------*/

    function initParallax() {
    $('#home').parallax("50%", 0.3);

  }
  initParallax();


  /* Back top
  -----------------------------------------------*/
  
  $(window).scroll(function() {
        if ($(this).scrollTop() > 200) {
        $('.go-top').fadeIn(200);
        } else {
          $('.go-top').fadeOut(200);
        }
        });   
        // Animate the scroll to top
      $('.go-top').click(function(event) {
        event.preventDefault();
      $('html, body').animate({scrollTop: 0}, 300);
      })

//
$('#mainsliders').owlCarousel({
    loop: true,
    nav: false,
    items: 1,
    autoHeight:false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    navText: ['<i class="fa fa-angle-left"></i>', 
    '<i class="fa fa-angle-right"></i>'],
    dots: true
  });


  //home_works
$('#home_works').owlCarousel({
    loop: true,
    nav: false,
    items: 3,
    margin:30,
    autoHeight:false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    navText: ['<i class="fa fa-angle-left"></i>', 
    '<i class="fa fa-angle-right"></i>'],
    dots: true
  });

  //
$('#testimonials').owlCarousel({
    loop: true,
    nav: false,
    items: 1,
    autoHeight:false,
    animateOut: 'fadeOut',
    animateIn: 'fadeIn',
    navText: ['<i class="fa fa-angle-left"></i>', 
    '<i class="fa fa-angle-right"></i>'],
    dots: true
  });

    /*
   *   Nicescroll 
   **/

    jQuery("body").niceScroll({
    cursorcolor:"#F7C200",
    cursorborder:"0px",
    cursorwidth :"8px",
    zindex:"9999"
  });
});
